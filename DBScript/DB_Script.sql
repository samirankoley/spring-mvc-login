drop database spring_db;
create database spring_db;
use spring_db;

create table users(id int primary key auto_increment, user_name text, user_password text);

insert into users(user_name,user_password) values('u001','p001');
insert into users(user_name,user_password) values('u002','p002');
insert into users(user_name,user_password) values('u003','p003');